require 'test_helper'

class WelcomeControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
        assert_select "title", "Inicio | Supermercado"

  end

  test "should get help" do
    get :help
    assert_response :success
        assert_select "title", "Ajuda | Supermercado"

  end
  
    test "should get about" do
    get :about
    assert_response :success
    assert_select "title", "Sobre | Supermercado"

  end


end
