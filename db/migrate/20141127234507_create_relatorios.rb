class CreateRelatorios < ActiveRecord::Migration
  def change
    create_table :relatorios do |t|
      t.text :content
      t.integer :produto_id

      t.timestamps null: false
    end
  end
end
