class CreateFuncionarios < ActiveRecord::Migration
  def change
    create_table :funcionarios do |t|
      t.string :name
      t.string :sexo
      t.string :email
      t.string :data
      t.integer :idade
      t.integer :salario

      t.timestamps null: false
    end
  end
end
