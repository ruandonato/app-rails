class Relatorio < ActiveRecord::Base
      belongs_to :produtos
      validates :content, length: { maximum: 140 }

end
