json.array!(@relatorios) do |relatorio|
  json.extract! relatorio, :id, :content, :produto_id
  json.url relatorio_url(relatorio, format: :json)
end
