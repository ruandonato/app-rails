json.array!(@funcionarios) do |funcionario|
  json.extract! funcionario, :id, :name, :sexo, :email, :data, :idade, :salario
  json.url funcionario_url(funcionario, format: :json)
end
