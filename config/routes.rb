Rails.application.routes.draw do
  get 'help'    => 'welcome#help'
  get 'about'   => 'welcome#about'
  get 'contact' => 'welcome#contact'
  get 'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

  get 'users'    => 'users#new'
  resources :relatorios
  resources :produtos
  resources :funcionarios
  resources :users

  root 'welcome#home'
end
